import asyncio
import zmq
import time
context = zmq.Context()
#GET from server
socket = context.socket(zmq.SUB)
socket.bind("ipc:///home/homer/Projects/WC/data/object")
socket.setsockopt(zmq.SUBSCRIBE, b"WHOP_")
socket.setsockopt(zmq.SUBSCRIBE, b"PING")
socket_push_downstream = context.socket(zmq.PUB)
socket_push_downstream.connect("ipc:///home/homer/Projects/WC/data/makeup")
#PUSH back to server
socket_push_upstream = context.socket(zmq.PUB)
socket_push_upstream.connect("ipc:///home/homer/Projects/WC/data/server")
#GET from client
socket_get = context.socket(zmq.SUB)
socket_get.bind("ipc:///home/homer/Projects/WC/data/cinfo")
socket_get.setsockopt(zmq.SUBSCRIBE, b"PING")
socket_get.setsockopt(zmq.SUBSCRIBE, b"cchara_")
#PUSH to client
socket_push = context.socket(zmq.PUB)
socket_push.connect("ipc:///home/homer/Projects/WC/data/cdo")


#Initiate the async loop
looper = asyncio.get_event_loop()
#define them
@asyncio.coroutine
def socketlistener():
	while True:
		mess = '_'
		print("RUN KITCHEN EARS")
		#time.sleep(1)
		mess = socket.recv()
		if mess == b"WHOP_":
			print("SHIT, SOMEONE WOKE THE KRACKEN")
			socket_push.send(b"FHOP_" + mess)
			looper.stop()
			sys.exit(0)
		elif mess == b"PING":
			print("PONG")
			time.sleep(1)
			print("SENDING THAT PING DOWNSTREAM")
			socket_push.send(mess)
			yield from socketgetlistener()
			return
		else:
			print("GOT SOME OTHER BULLSHIT. IGNORING")
			return
		mess = '_'
	pass

@asyncio.coroutine
def socketgetlistener():
	while True:
		mess = '_'
		print("RUN CLIENT LISTENER")
		time.sleep(1)
		mess = socket_get.recv()
		#begin system functions
		if mess == b"PING":
			print("OH, GOOD, THEY'RE STILL HERE")
			#print("PONG")
			socket_push_upstream.send(mess)
			yield from socketlistener()
			return
		#begin game functions
		mess = mess.split(b'~')
		if mess[0] == b"cchara_NAME":
			print("HEH, NOOB")
			print("FILE THE NOOBIE WITH THE BOOBIES")
			socket_push_upstream.send(mess[0] + b'~' + mess[1])
			socket_push.send(b"PING")
			mess = yield from socketgetlistener()
		elif mess[0] == b"cchara_LOGIN":
			print("WE'VE GOT LOGIN SIIGN")
			socket_push_upstream.send(mess[0])
			return
		mess = '_'
	pass


asyncio.ensure_future(socketgetlistener())
asyncio.ensure_future(socketlistener())
looper.run_forever()
