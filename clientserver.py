
import asyncio
import zmq
import time
from types import *
from sqlalchemy import func
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
#TURN ECHO ON FOR WALL OF SHIT
engine = create_engine('sqlite:///:memory:', echo=False)
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

from sqlalchemy import Column, Integer, String, Boolean
class Player (Base):
	__tablename__ = 'pc'
	active = Column(Boolean)
	room_id = Column(Integer)
	id = Column(Integer, primary_key=True)
	name = Column(String)
	fullname = Column(String)
	password = Column(String)
	statmort = Column(String)
	statdiva = Column(String)
	statelle = Column(String)
	charatype = Column(String)
Base.metadata.create_all(engine)



context = zmq.Context()
#PUSH to the fiddleman
socket_upstream = context.socket(zmq.PUB)
socket_upstream.connect("ipc:///home/homer/Projects/WC/data/cinfo")
#GET from forwarding server
socket_upstream_get = context.socket(zmq.SUB)
socket_upstream_get.bind("ipc:///home/homer/Projects/WC/data/cdo")
#GET ALL THE THINGS
socket_upstream_get.setsockopt(zmq.SUBSCRIBE, b'FHOP_')
socket_upstream_get.setsockopt(zmq.SUBSCRIBE, b'PING')
#GET Player input #locally# to get commands
socket_client_input = context.socket(zmq.SUB)
socket_client_input.bind("ipc:///home/homer/Projects/WC/data/cinput")
socket_client_input.setsockopt(zmq.SUBSCRIBE, b"PING")
socket_client_input.setsockopt(zmq.SUBSCRIBE, b"cchara_")

#initiate the async loop
looper = asyncio.get_event_loop()
#PUSH info to the player
socket_client_output = context.socket(zmq.PUB)
socket_client_output.connect("ipc:///home/homer/Projects/WC/data/coutput")



#set up the listener function for the Player
@asyncio.coroutine
def socketlistener():
	while True:
		print("RUN PLAYER LISTENER")
		mess = "_"
		time.sleep(1)
		mess = socket_client_input.recv()
		if mess == b"PING":
			print("PONG")
			print("GOT PING FROM PLAYER")
			time.sleep(1)
			socket_client_output.send(mess)
			print("PING THE FIDDLEMAN")
			socket_upstream.send(mess)
			return
		elif mess == b"cchara_LOGIN":
			print("SHIT! SOMEONE WANTS TO PLAY!")
			print("INFORM THE FIDDLEMAN")
			socket_upstream.send(mess)
			return
		else:
			print("SHIT, MUST BE PLAYER INPUT")
			print("PARSE, PAAAARRRRRRSE")
		#mess = mess.decode('utf-8')
		mess = mess.split(b'~')
		if mess[0] == b"cchara_NAME":
			mess[1] = mess[1].decode('utf-8')
			print("NEW CHARACTER NAMED " + str(mess[1]))
			session.add( Player(name=mess[1]))
			session.commit()
			#PING THE FIDDLEMAN AND FIND OUT HOW TO PUSH YOUR SHIT UP
			socket_upstream.send(mess[0] + b'~' + bytes(mess[1], 'utf-8'))
			return
		mess = "_"
	pass






#set up the listener func for the forwarder
@asyncio.coroutine
def socketgetlistener():
	while True:
		mess = "_"
		print("RUN GET FIDDLEMAN")
		time.sleep(1)
		#mess = yield from socket_upstream_get.recv_multipart()
		mess = socket_upstream_get.recv()
		#mess = mess.split(b'~')
		if mess == b"PING":
			print("PONG")
			time.sleep(1)
			print("GOT PING FROM FIDDLEMAN")
			socket_client_output.send(mess)
			mess = yield from socketlistener()
			#socket_upstream.send(mess)
			#yield from socketgetlistener()
			yield
		elif mess == b"WHOP_":
			print("WHOP")
			print("ALRIGHT, WHO PISSED OFF THE ADMIN")
			#socket_upstream.send_string(mess)
			#act on the WHOP and see what's up
			print("SHUT IT DOWN.\nSHUT IT ALL DOWN")
			looper.stop()
			sys.exit(0)
		else:
			print("GOT " + mess + " FROM SERVER")
			print("WHAT DO?")
			socket_upstream.send_string(mess)

asyncio.ensure_future(socketlistener())
asyncio.ensure_future(socketgetlistener())
#looper.call_soon(socketlistener())
#looper.call_soon(socketgetlistener())

looper.run_forever()
