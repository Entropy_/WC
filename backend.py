import asyncio
import time
from sqlalchemy import func
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
#TURN ECHO OFF FOR NON BLINDING WALL OF SHIT
engine = create_engine('sqlite:///:memory:', echo=True)
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)
session = Session()
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from sqlalchemy import Column, Integer, String, Boolean
class Room (Base):
	__tablename__ = 'rooms'
	users = relationship("User", back_populates="rooms")
	id = Column(Integer, primary_key=True)
	header = Column(String)
	desc = Column(String)
	#makeup = Column(String) This should be a seperate table

class User (Base):
	__tablename__ = 'users'
	active = Column(Boolean)
	room_id = Column(Integer, ForeignKey('rooms.id'))
	rooms = relationship("Room", back_populates="users")
	id = Column(Integer, primary_key=True)
	name = Column(String)
	fullname = Column(String)
	password = Column(String)
	statmort = Column(String)
	statdiva = Column(String)
	statelle = Column(String)
	charatype = Column(String)
Base.metadata.create_all(engine)

#begin adding rooms
session.add( Room(id=0, header='<<<<<=====THE DIAMOND GATE=====>>>>>', \
	desc='You stand before a huge gate made of shimmering crystals.') )
session.add( Room(id=1, header='++++======Big Berthas House of Bang======++++',	desc=' The smell of stale beer and blood creeps in to your nostrils\n nothing seems to look right. The tables are all off-kilter, the\n bar is not quite polished enough. But it is home, and it is where you\n belong') )

session.add_all([
	User(name='groot', fullname='I am Groot', password='iamgroot', \
	active=True, room_id=0, statmort="18_18_18_18_18_18", statdiva=\
	"1_1_1", statelle="15_15_15_15", charatype="elle"),
	User(name='wendy', fullname='Wendy Williams', password='foobar', \
	active=False, room_id=1),
	User(name='mary', fullname='Mary Contrary', password='xxg527', \
	active=False, room_id=1),
	User(name='fred', fullname='Fred Flinstone', password='blah', \
	active=False, room_id=1)])

session.commit()

#define commands
#TODO add user levels to access controls
def whoami():
	instance = session.query(User).filter(User.active == True).one()
	#print(instance.fullname)
	return instance.fullname

def assignuser(user, cuser):
	instance = session.query(User).filter(User.name == user).one()
	instance.active = False
	instance = session.query(User).filter(User.name == cuser).one()
	instance.active = True
	return instance.name

def who():
	for instance in session.query(User).order_by(User.id):
		#print(instance.name, instance.fullname)
		return instance.name, instance.fullname
def examine(person):
	for user in  session.query(User).filter(User.name == person):
		#print(user.fullname)
		return user.fullname
def countusers(): session.query(func.count(User.id)).scalar()

def look(*arg):
	room = session.query(User).filter(User.active == True).one()
	room = session.query(Room).filter(Room.id == room.room_id).one()
	#print(room.header)
	return room.header, room.desc
	#print(room.desc)
def score():
	chara = session.query(User).filter(User.active == True).one()
	if chara.charatype == 'elle':
		return chara.statelle
	elif chara.charatype == 'diva':
		return chara.statdiva
	elif chara.charatype == 'mort':
		return chara.statmort
	
def whop():
	socket.send(b'WHOP_')
	#TODO
	#add WHOP commands
def textinput(arg):
	count = 0
	for keyword in arg:
		if keyword == 'who':
			return who()
		elif keyword == 'examine':
			return examine(arg[count + 1])
		elif keyword == 'whoami':
			return whoami()
		elif keyword == 'cuser':
			return assignuser(arg[count + 1], arg[count + 2])
		elif keyword == 'look':
			return look()
		elif keyword == 'score':
			return score()
		count = count + 1

#define the zmq to talk to the server
import zmq
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.connect("ipc:///home/homer/Projects/WC/data/object")
#ALL SOCKETS IN SAME PLACE
#TODO

def interr():
	time.sleep(1)
	choice = raw_input("Enter your data\n")
	choice = choice.split(' ')
	print(choice)
	chooser = textinput(choice)
	socket.send_string(chooser)
	print("Sending string")
socket_GET = context.socket(zmq.SUB)
socket_GET.bind("ipc:///home/homer/Projects/WC/data/server")
socket_GET.setsockopt(zmq.SUBSCRIBE, b"PING")
socket_GET.setsockopt(zmq.SUBSCRIBE, b"cchara_")
looper = asyncio.get_event_loop()




@asyncio.coroutine
def socketlistener():
	while True:
		print("I suppose I should get out of bed today")
		mess = '_'
		mess = socket_GET.recv()
		if mess == b"PING":
			print("Well well well...")
			print("Pong")
			socket.send(mess)
			time.sleep(1)
		elif mess != '_':
			print("meh")
			#TODO
			#handle cchar queries
			#TODO
			time.sleep(1)
			print("I guess I should")
			socket.send(b"PING")
			print("PONG")
		else:
			time.sleep(1)
		mess = '_'

asyncio.ensure_future(socketlistener())
looper.run_forever()
